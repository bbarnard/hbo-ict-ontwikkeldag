package btree;

import java.util.ArrayList;


public class BTree {
	public static final int degree = 2;
	public int height = 0;
	private Node root;

	public BTree() {
		root = new Node("root node");
	}

	/** improved algorithm by Mond & Raz 1985. 
	 * descent into the tree, splitting every full node it encounters along the way.
	 * @param key
	 */
	
	public final void insert (int key) {
		if (root.getKeys().size()==0) {
			root.setHeight(height); // initial node of the whole tree.
			root.getKeys().add(key);
			return;
		}

		if (root.isFull()) {
			height++;
			Node newRoot = new Node(" root at height "+height);
			newRoot.setHeight(height);
			newRoot.setChildNode(root);
			split(newRoot, root, 0);
			insert_non_full(newRoot, key);
			root = newRoot;
		}
		else insert_non_full(root, key);	
	}

	private final void insert_non_full(Node node, int key) {
		if (node.isLeaf()) {
			node.getKeys().add(key);
		} else {
			int i=0;
			while (i<node.getKeys().size() && key>node.getKeys().get(i)) {
				i++;
			}
			
			Node next = node.getChildNodes().get(i);
			if (next.isFull()) {
				split(node, next, i);
				if (key>node.getKeys().get(i)) i++;
				next = node.getChildNodes().get(i);
			}
			
			insert_non_full(next, key);
					
		}
	}

	/**
	 * Using a simple lineair search, this method finds the smallest index i such that
	 * searchKey ≤ keys[i]. If this is the searched key, this is returned, otherwise the
	 * method recurse to search the appropriate child tree.
	 * Cormen et al. 2005, pp.441-442
	 * 
	 * @param startNode The node of the (sub)tree where the search is to start from.
	 * @param searchKey The key to be searched.
	 * @return the index of the key in the keys of the node that contains the searched key, -1 otherwise.
	 */
	public final int search (Node startNode, int searchKey) {
		int i=0;
		while (i<startNode.getKeys().size() && searchKey>startNode.getKeys().get(i)) {
			i++;
		}

		if (i<startNode.getKeys().size() && searchKey==startNode.getKeys().get(i)) {
			System.out.println("Found @ " + startNode.getSattelite());
			return i;
		}

		if (startNode.isLeaf()) return -1;
		else return search (startNode.getChildNodes().get(i), searchKey);
	}
	
	//Overloaded method for demonnstration purposes
	public final int search(int searchKey) {
		return this.search (root, searchKey);
	}



	/**
	 * This operation splits a full childnode (with n[x]=2t-1) around its median
	 * into two nodes having t-1 keys. The median key moves up into the original
	 * node's parent (this node) to identify the dividingpoint between the two new trees.
	 * the new node is added to the parent (this node) at the appropriate location.
	 * Cormen et al. 2005, pp443f.
	 * 
	 * Instead of keeping the original node, we actually create <i>two</i> new nodes,
	 * one left and one right, and replace the original one with the new left node.
	 * Also we added an extra property, <i>height</i>, to the nodes in order to show their heights.
	 * 
	 * @param parentNode The parent node of this (sub)tree.
	 * @param childNode The full node to be split.
	 * @param index The full node is the index'th childnode of its parent.
	 */

	public  final void split (Node parentNode, Node childNode, int index) {
		Node newRightNode = new Node("Newly created right node");
		Node newLeftNode = new Node("Newly created left node");

		SortedList<Integer> newKeysRight = new SortedList<>();
		ArrayList<Node> newChildNodesRight= new ArrayList<>();
		SortedList<Integer> newKeysLeft = new SortedList<>();
		ArrayList<Node> newChildNodesLeft = new ArrayList<>();
		
		//first, copy and paste the child nodes if this node is not a leaf
		//this needs a separate pass, as the number of loops is n[x] and not n[x]-1 
		if (!childNode.isLeaf()) {
			for (int j=0; j<=degree-1; j++) {
				newChildNodesLeft.add(childNode.getChildNodes().get(j));
				newChildNodesRight.add(childNode.getChildNodes().get(degree+j));
			}
		}

		// now copy and paste the approriate keys to the new node
		// if in a for: to be refactored!
		int i;
		for (i=0; i<degree-1; i++) {
			newKeysLeft.add(childNode.getKeys().get(i));
			newKeysRight.add(childNode.getKeys().get(degree+i));
		}

		// when exiting the loop above, 
		// i = degree-1, which is the median of the full node.
		int median = childNode.getKeys().get(i);

		// adding the median to this node's keys (moving it up)
		// making use of the overloaded method add(index, Object) of ArrayList
		parentNode.getKeys().add(index, Integer.valueOf(median));

		//adding the approriate values to the new childnodes
		newLeftNode.setKeys(newKeysLeft);
		newLeftNode.setChildNodes(newChildNodesLeft);
		newLeftNode.setHeight(childNode.getHeight());
		
		newRightNode.setKeys(newKeysRight);
		newRightNode.setChildNodes(newChildNodesRight);
		newRightNode.setHeight(childNode.getHeight());

		//replacing the existing childNode with the new (left) node
		parentNode.getChildNodes().set(index, newLeftNode);

		//adding the newly created (right) node to the childnodes
		parentNode.getChildNodes().add(index+1, newRightNode);
	}
	
	public String showTree() {
		return showChildren(root);
	}
	
	private String showChildren(Node n) {
		StringBuilder sb = new StringBuilder();
		sb.append(n.toString());
		
		for (Node child: n.getChildNodes()) {
			sb.append(showChildren(child));
		}
		sb.append("\n");
		return sb.toString();
	}
	
}
