package btree;


public class BTreeDemo {
	
	public static void BTreeCreateDemo() {
		BTree btree = new BTree();
		
		// replace the for-loop below with an IntStream
		for (int i=1; i<11; i++) {
			btree.insert(i);
		}
		System.out.println(btree.showTree());
		
		int demo = btree.search(6);
		System.out.println(demo);
	
	}
	
	
	public static void main(String[] args) {
		BTreeDemo.BTreeCreateDemo();
	}

}
