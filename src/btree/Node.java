package btree;

import java.util.ArrayList;


public class Node {
	private SortedList<Integer> keys = new SortedList<Integer>();
	private ArrayList<Node> childNodes = new ArrayList<Node>();
	private String sattelite;
	private int height;
	
	//constructors
	public Node() { this("Default node"); }
	public Node(String title) { this.sattelite = title; }
	
	//getters and setters
	public void setKeys(SortedList<Integer> vals) { this.keys = vals; }
	public SortedList<Integer> getKeys() { return this.keys; }
	
	public void setChildNode(Node node) { childNodes.add(node); }
	public void setChildNodes(ArrayList<Node> nodes) { this.childNodes = nodes; }
	public ArrayList<Node> getChildNodes() { return this.childNodes; }
	
	public void setSattelite(String str) { sattelite = str; }
	public String getSattelite() { return this.sattelite; }
	
	public void setHeight(int h) { this.height = h; }
	public int getHeight() { return this.height; }
	public void increaseHeight() { this.height++; }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("This is node " + sattelite + "\n");
		sb.append("We are at height " +height+ "\n");
		sb.append("Here come my keys: ");
		String keys = "";
		String del = "";
		
		for (int i: this.keys) {
			keys += del + i;
			del = ",";
		}
		sb.append(keys+"\n");
		sb.append("Number of childnodes: " +childNodes.size()+"\n");
		sb.append("\n");
		return sb.toString();
	}

	
	//convenience methods
	protected boolean isFull() {
		return keys.size() == 2*BTree.degree -1;
	}
	
	protected boolean isLeaf() {
		return childNodes.size()==0;
	}
}
