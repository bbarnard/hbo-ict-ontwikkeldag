package btree;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class is only for demonstration purposes of the functionality of Java 8,
 * created for the conference 24 March. No guarantee is given to its working or its
 * performance (which is, actually, quite bad). 
 * For better implementations of sorted lists in Java, one should use <i>Bag</i> or <i>Set</i>,
 * use the <i>Collections.sort()</i> method, or wrap the list in a <i>PriorityQueue</i>.
 * 
 * @author Bart Barnard
 * @since March 13, 2015
 *
 * @param <E> The type of elements that this list holds.
 */

public class SortedList<E> extends AbstractList<E> {
	private ArrayList<E> internalList = new ArrayList<E>();
	
	public SortedList() {
		super();
	}
	
	public SortedList(List<E> els) {
		super();
		internalList = new ArrayList<E>(els);
	}
	
	@Override
	public E set(int index, E element) {
		E previous = internalList.get(index);
		internalList.set(index, element);
		return previous;
	}
	
	// only overriding for demonstration purposes.
	@Override
	public void add(int position, E element) {
		this.add(element);
	}
	
	@Override
	public boolean add(E element) {
		boolean rv = internalList.add(element);
		Collections.sort(internalList, null);
		return rv;
	}

	@Override
	public E get(int index) {
		return internalList.get(index);
	}

	@Override
	public int size() {
		return internalList.size();
	}
	
	
	

}
