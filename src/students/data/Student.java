package students.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Student {
	private int id;
	private String name;
	private String city;
	private int age;
	private Class clazz;
	private Map<Course, List<Double>> course_to_marks;

	public Student(String name, String city, int age, Class clazz) {
		this.name = name;
		this.city = city;
		this.age = age;
		this.clazz = clazz;
		course_to_marks = new HashMap<Course, List<Double>>();
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public int getAge() {
		return age;
	}

	public Class getClazz() {
		return clazz;
	}

	public Map<Course, List<Double>> getCourseToMarks() {
		return Collections.unmodifiableMap(course_to_marks);
	}

	public List<Double> getMarksForCourse(Course course) {
		List<Double> marks = course_to_marks.get(course);
		return marks != null ? new ArrayList<Double>(marks) : new ArrayList<Double>();
	}

	public void setMark(Course course, Double mark) {
		List<Double> marks = course_to_marks.get(course);
		if (marks == null) {
			marks = new ArrayList<Double>();
			course_to_marks.put(course, marks);
		}
		marks.add(mark);
	}

	@Override
	public String toString() {
		return String.format("Person{name='%s', city='%s', age=%d, class='%s'}", name, city, age, clazz);
	}

	// TODO: override equals method
}
