package students.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Data {

	private static Data instance;

	private List<Student> students = new ArrayList<Student>();

	public Data() {
		populate();
	}

	public static Data getInstance() {
		if (instance == null) {
			instance = new Data();
		}
		return instance;
	}

	public List<Student> getStudents() {
		return new ArrayList<Student>(students); // we need a copy!
	}

	private List<String> readLines(String filename) {
		List<String> names = new ArrayList<String>();
		Path path = Paths.get(filename);
		try {
			Files.lines(path).forEach(s -> names.add(s));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return names;
	}

	public void populate() {
		Random rand = new Random();
		List<String> names = readLines("students.txt");
		List<String> cities = readLines("cities.txt");
		for (String name : names) {
			Student newStudent = new Student(name, cities.get(rand.nextInt(cities.size())), rand.nextInt(83) + 17, Class.values()[rand.nextInt(Class.values().length)]);
			students.add(newStudent);

			boolean superStudent = rand.nextInt(2) == 1;
			int courseCount;
			if (superStudent) {
				courseCount = Course.values().length;
			} else {
				courseCount = Course.values().length / 2 + (int) Math.round(rand.nextDouble() * (Course.values().length / 2));
			}
			for (int courseIndex = 0; courseIndex < courseCount; courseIndex++) {
				Course course = Course.values()[superStudent ? courseIndex : rand.nextInt(Course.values().length)];
				int markCount = rand.nextInt(3) + 1;
				for (int markIndex = 0; markIndex < markCount; markIndex++) {
					double mark;
					if (superStudent) {
						mark = rand.nextDouble() * 4 + 6;
					} else {
						mark = rand.nextDouble() * 7 + 3;
					}
					newStudent.setMark(course, mark);
				}
			}
		}
	}
}
