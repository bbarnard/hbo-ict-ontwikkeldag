package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Course;
import students.data.Student;

public class D9CalculateRendement extends Exercise<Double> {

	@Override
	public List<Double> imperative() {
		List<Student> list = data.getStudents();
		int total = list.size();
		int amount = 0;
		for (Student student : list) {
			boolean sufficient = true;
			for (Course course : Course.values()) {
				List<Double> marks = student.getMarksForCourse(course);
				if (marks.isEmpty()) {
					sufficient = false;
				}
				for (Double mark : marks) {
					if (mark < 5.50) {
						sufficient = false;
					}
				}
			}
			if (sufficient)
				amount++;
		}
		List<Double> result = new ArrayList<Double>();
		result.add((double) amount / ((double) total / 100));
		return result;
	}

	@Override
	List<Double> functional() {
		List<Double> result = new ArrayList<Double>();

		/*
		 * TODO bereken het rendement van de hele opleiding over alle studenten
		 * voor het gemak verstaan we onder rendement het percentage studenten
		 * dat voor alle vakken tenminste 1 cijfer heeft en waarvoor alle
		 * cijfers voldoende zijn
		 */

		return result;
	}

	/*
	 * Additional task: calculate average mark for a particular course
	 */

	public static void main(String[] args) {
		(new D9CalculateRendement()).run(false);
	}

}
