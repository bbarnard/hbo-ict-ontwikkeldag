package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Course;
import students.data.Student;

public class D7FilterStudentsByInsufficientMarksForProgramming extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		List<Student> result = new ArrayList<Student>();
		for (Student student : list) {
			List<Double> marks = new ArrayList(student.getMarksForCourse(Course.OOP1));
			marks.addAll(student.getMarksForCourse(Course.OOP2));
			marks.addAll(student.getMarksForCourse(Course.FUNCTIONAL_PROGRAMMING));
			marks.addAll(student.getMarksForCourse(Course.PHP));
			for (Double mark : marks) {
				if (mark < 5.50) {
					result.add(student);
					break;
				}
			}
		}
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton to return the students with one or more
		// insufficient marks for any of the programming courses

		return students;
	}

	/*
	 * Additional task: implement another functional version by implementing
	 * your own functional Predicate and your own functional Function
	 * 
	 * Also use your Predicate and Function to simplify the lambda expression
	 * used in the main method
	 */

	public static void main(String[] args) {
		(new D7FilterStudentsByInsufficientMarksForProgramming()).run(false,
				s -> s.getMarksForCourse(Course.OOP1).toString() + s.getMarksForCourse(Course.OOP2) + s.getMarksForCourse(Course.PHP) + s.getMarksForCourse(Course.FUNCTIONAL_PROGRAMMING)); // TODO:
		// pass
		// function
		// to
		// print
		// marks);
	}

}
