package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Course;
import students.data.Student;

public class D8IncreaseInsufficientMarksForCommuncation extends Exercise<Double> {

	@Override
	public List<Double> imperative() {
		List<Student> list = data.getStudents();
		List<Double> result = new ArrayList<Double>();
		for (Student student : list) {
			for (Double mark : student.getMarksForCourse(Course.COMMUNICATION)) {
				if (mark < 5.50) {
					result.add(mark * 1.1);
					break;
				}
			}
		}
		return result;
	}

	@Override
	List<Double> functional() {
		List<Double> result = new ArrayList<Double>();

		/*
		 * TODO Return a list with all insufficient marks for communication
		 * increased with 10%
		 */
		return result;
	}

	public static void main(String[] args) {
		(new D8IncreaseInsufficientMarksForCommuncation()).run(false);
	}

}
