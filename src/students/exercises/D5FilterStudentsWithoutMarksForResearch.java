package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Course;
import students.data.Student;

public class D5FilterStudentsWithoutMarksForResearch extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		List<Student> result = new ArrayList<Student>();
		for (Student student : list) {
			if (student.getMarksForCourse(Course.RESEARCH1).isEmpty() && student.getMarksForCourse(Course.RESEARCH2).isEmpty()) {
				result.add(student);
			}
		}
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton to return the students without any mark
		// for research

		return students;
	}

	public static void main(String[] args) {
		(new D5FilterStudentsWithoutMarksForResearch()).run(false); // TODO:
																	// pass
																	// function
																	// to
																	// print
																	// marks);
	}
}