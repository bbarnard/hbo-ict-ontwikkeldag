package students.exercises;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import students.data.Data;
import students.data.Student;

public abstract class Exercise<ResultDataType> {
	protected Data data = Data.getInstance(); // for convenience
	protected List<Student> persons = data.getStudents();

	abstract List<ResultDataType> imperative();

	abstract List<ResultDataType> functional();

	/*
	 * Implementation of computeDifference and isOrderTheSame employs the equals
	 * method which should imply identity in content, not necessarily identity
	 * of instancess
	 */
	protected List<ResultDataType> computeDifference(List<ResultDataType> list1, List<ResultDataType> list2) {
		return list1.stream().filter(item1 -> !list2.stream().anyMatch(item2 -> item1.equals(item2))).collect(Collectors.toList());
	}

	protected boolean isOrderTheSame(List<ResultDataType> list1, List<ResultDataType> list2) {
		// assumes the length and content is the same
		for (int i = 0; i < list1.size(); i++) {
			if (!list1.get(i).equals(list2.get(i))) {
				return false;
			}
		}
		return true;
	}

	protected void printTitle() {
		System.out.println("*** Question " + this.getClass().getSimpleName() + " ***\n");
	}

	public void run(boolean orderRequired) {
		run(orderRequired, s -> "");
	}

	public void run(boolean orderRequired, Function<ResultDataType, String> extraOutput) {
		printTitle();

		List<ResultDataType> imperativeResults = imperative();
		List<ResultDataType> functionalResults = functional();

		if ((imperativeResults == null) || (functionalResults == null)) {
			System.out.println("Your implementation should NOT return a null pointer!");
			return;
		}

		List<ResultDataType> missing = computeDifference(imperativeResults, functionalResults);
		List<ResultDataType> superfluous = computeDifference(functionalResults, imperativeResults);

		if (missing.isEmpty() && superfluous.isEmpty()) {

			if (orderRequired && !isOrderTheSame(imperativeResults, functionalResults)) {
				System.out.println("Unfortunately, the order of your functional output is incorrect.");
			} else {
				System.out.println("Congratulations! Output of your functional version matches the output of the imperative version.");
				System.out.println("Please mind that your implementation can still be wrong (why?).");
			}
		} else {
			if (!missing.isEmpty()) {
				System.out.println("Unfortunately, output of your functional version is MISSING the following entries:");
				missing.forEach(o -> System.out.println(o.toString()));
				System.out.println("");
			}

			if (!superfluous.isEmpty()) {
				System.out.println("Unfortunately, output of your functional version contains the following SUPERFLUOUS entries:");
				superfluous.forEach(o -> System.out.println(o.toString()));
				System.out.println("");
			}
			System.out.println("Try again, yes we can!");
		}

		System.out.println("\nPress Enter to print your outcome:\n");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		functionalResults.forEach(o -> System.out.println(o.toString() + extraOutput.apply(o)));
		if (functionalResults.isEmpty())
			System.out.println("EMPTY");
	}

}
