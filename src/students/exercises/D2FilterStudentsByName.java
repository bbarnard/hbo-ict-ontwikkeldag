package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Student;

public class D2FilterStudentsByName extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> result = new ArrayList<Student>();
		for (Student student : data.getStudents()) {
			if (student.getName().startsWith("P")) {
				result.add(student);
			}
		}
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();
		List<Student> result = new ArrayList<Student>();

		// TODO functional implementaton of filter by names starting with a P

		return result;
	}

	public static void main(String[] args) {
		(new D2FilterStudentsByName()).run(false);
	}

}
