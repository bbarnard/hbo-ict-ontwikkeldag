package students.exercises;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import students.data.Class;
import students.data.Student;

public class D4FilterStudentsByAgeClassAndSort extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		List<Student> result = new ArrayList<Student>();
		for (Student student : list) {
			if (student.getAge() > 33 && (student.getClazz() == Class.TI1A || student.getClazz() == Class.TI1B)) {
				result.add(student);
			}
		}
		result.sort(new Comparator<Student>() {
			public int compare(Student student1, Student student2) {
				return student1.getName().compareTo(student2.getName());
			}
		});
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton of filter by elderly students with age
		// > 33 and Class TI*, sort by name

		return students;
	}

	public static void main(String[] args) {
		(new D4FilterStudentsByAgeClassAndSort()).run(true);
	}
}
