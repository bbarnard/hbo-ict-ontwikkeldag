package students.exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D9XDiffSameOrderQuestion extends Exercise<String> {

	/*
	 * This is a more advanced task that most likely requires to look up
	 * information about the performance of streams and lambda expressions on
	 * the internet
	 */

	/*
	 * Returns true if the order of the two lists is the same. Assumes the
	 * content of the two lists is the same
	 */
	@Override
	protected boolean isOrderTheSame(List<String> list1, List<String> list2) {

		/*
		 * look at the imperative implementation in the superclass
		 * 
		 * TODO: implement a functional version
		 * 
		 * Which implementation would have the best performance?
		 */
		return false;
	}

	/*
	 * Returns the difference between list1 and list2 such that the returned
	 * list contains items that are part of list1, but not of list2.
	 */
	@Override
	protected List<String> computeDifference(List<String> list1, List<String> list2) {
		/*
		 * look at the functional implementation in the superclass
		 * 
		 * TODO: implement a functional version
		 * 
		 * Find other functional implementatons, compare them with respect to:
		 * 
		 * complexity and performance
		 * 
		 * Which implementation has best performance?
		 */

		return new ArrayList<String>();
	}

	// implementation to test the two functions above
	static List<String> testList1 = Arrays.asList("Piet", "Jan", "Klaas", "Arie");
	static List<String> testList2 = Arrays.asList("Piet", "Jan", "Klaas", "Arie");
	static List<String> testList3 = Arrays.asList("Piet", "Klaas", "Arie");
	static List<String> testList4 = Arrays.asList("Arie", "Klaas", "Piet");

	@Override
	public void run(boolean orderRequired) {
		printTitle();

		if (isOrderTheSame(testList1, testList2) && !isOrderTheSame(testList3, testList4)) {
			System.out.println("Congratulations! Implementation of isOrderTheSame seems correct.");
		} else {
			System.out.println("Unfortunately, your implementation of isOrderTheSame contains a bug.");
		}

		if (computeDifference(testList1, testList2).isEmpty() && computeDifference(testList3, testList4).isEmpty()) {
			List<String> diff = computeDifference(testList2, testList3);
			if ((diff.size() == 1) && (diff.get(0).equals("Jan"))) {
				diff = computeDifference(testList3, testList2);
				if ((diff.size() == 0)) {
					System.out.println("Congratulations! Implementation of computeDifference seems correct.");
					return;
				}
			}
		}
		System.out.println("Unfortunately, your implementation of computeDifference contains a bug.");
	}

	@Override
	List imperative() {
		return null;
	}

	@Override
	List functional() {
		return null;
	}

	public static void main(String[] args) {
		(new D9XDiffSameOrderQuestion()).run(true);
	}

}