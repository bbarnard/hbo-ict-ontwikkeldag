package students.exercises;

import java.util.ArrayList;
import java.util.List;

import students.data.Course;
import students.data.Student;

public class D6FilterStudentsByInsufficientMarksForCommunication extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		List<Student> result = new ArrayList<Student>();
		for (Student student : list) {
			for (Double mark : student.getMarksForCourse(Course.COMMUNICATION)) {
				if (mark < 5.50) {
					result.add(student);
					break;
				}
			}
		}
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton to return the students with one or more
		// insufficient marks for communication

		return students;
	}

	public static void main(String[] args) {
		(new D6FilterStudentsByInsufficientMarksForCommunication()).run(false, s -> s.getMarksForCourse(Course.COMMUNICATION).toString());

	}
}
