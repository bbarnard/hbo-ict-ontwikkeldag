package students.exercises;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import students.data.Student;

public class D3FilterStudentsByNameAndSort extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		List<Student> result = new ArrayList<Student>();
		for (Student student : list) {
			if (student.getName().startsWith("P")) {
				result.add(student);
			}
		}
		result.sort(new Comparator<Student>() {
			public int compare(Student student1, Student student2) {
				return student1.getName().compareTo(student2.getName());
			}
		});
		return result;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton of filter by names starting with 'P'
		// and sort by name

		return students;
	}

	public static void main(String[] args) {
		(new D3FilterStudentsByNameAndSort()).run(true);
	}

}
