package students.exercises;

import java.util.Comparator;
import java.util.List;

import students.data.Student;

public class D1SortStudentsByName extends Exercise<Student> {

	@Override
	public List<Student> imperative() {
		List<Student> list = data.getStudents();
		list.sort(new Comparator<Student>() {
			public int compare(Student student1, Student student2) {
				return student1.getName().compareTo(student2.getName());
			}
		});
		return list;
	}

	@Override
	public List<Student> functional() {
		List<Student> students = data.getStudents();

		// TODO functional implementaton of sort by name

		return students;
	}

	// TODO additonal task: modify both implementations such that students are
	// first sorted by name and secondly by city

	public static void main(String[] args) {
		(new D1SortStudentsByName()).run(true);
	}
}
